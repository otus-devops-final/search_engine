# Search Engine Infra

DevOps инфраструктура для микросервисного приложения поискового движка.

Для работы используются:
1. Google Cloud Platform
1. GitLab: репозиторий, Container Registry, CI/CD

## Из чего состоит приложение

- crawler - поисковый бот для сбора текстовой информации с веб-страниц и ссылок
(на основе https://github.com/express42/search_engine_crawler)
- ui - веб-интерфейс поиска слов и фраз на проиндексированных ботом сайтах
(на основе https://github.com/express42/search_engine_ui)
- mongo - база данных для хранения индекса (MongoDB 3.4) 
- rabbit - менеджер очередей сообщений (RabbitMQ 3.8) 

## Подготовка инфраструктуры в GCP

Инфраструктура включает в себя три окружения (dev/stage/prod) и вспомогательный
хост builder для запуска тестов и сборки образов. Каждое из окружений состоит из
одного хоста, на котором работают сервисы приложения и сервисы мониторинга.

В данном разделе описан процесс создания и настройки инфраструктуры в GCP
при помощи Terraform и Ansible. В качестве альтернативы возможен вариант 
[ручной настройки](wiki/manual_infra.md) при помощи `gcloud` и `docker-machine`.

### Создание ресурсов при помощи Terraform

1. Создать проект в GCP, например: otus-12345.

1. Сгенерировать SSH-ключи для пользователя appuser:
    ```shell script
    ssh-keygen -t rsa -f ~/.ssh/appuser -C appuser -P ""
    ```

1. Создать `infra/terraform/terraform.tfvars`, прописав в нём название проекта.
За основу взять `infra/terraform/terraform.tfvars.example`.

1. Добавить правила файервола и создать хосты при помощи Terraform:
    ```shell script
    cd infra/terraform
    terraform init
    terraform apply
    ```

### Первичная настройка при помощи Ansible

1. [Создать ключ](https://console.cloud.google.com/iam-admin/serviceaccounts)
для сервисного аккаунта и сохранить его как `infra/ansible/service_account.yml`.

1. Создать  `infra/ansible/inventory.gcp.yml`, прописав в нём название проекта.
За основу взять `infra/ansible/inventory.gcp.yml.example`.

1. Установить на машинки Docker и зарегистрировать необходимые GitLab Runner'ы.
    ```shell script
    cd infra/ansible
    ansible-playbook playbooks/infra.yml -e gitlab_token=<GITLAB_TOKEN>
    ``` 

## CI/CD при помощи GitLab CI

Сборка/деплой докер образов сервисов приложения и сервисов мониторинга
производится при помощи GitLab CI. В качестве альтернативы возможен вариант 
[ручной сборки и деплоя](wiki/manual_deploy.md).

1. Unit-тесты для приложений crawler и ui запускаются в GitLab CI на каждый
коммит.

1. Docker образы собираются и пушатся в хранилище при каждом коммите в master.

1. Деплой на dev происходит автоматически при каждом коммите в master.

1. Для деплоя на stage или prod необходимо создать тег с версией (например,
1.0.0). Деплой запускается вручную по кнопке в GitLab.

## Мониторинг

1. Prometheus будет доступен по адресу `http://HOST_IP:9090`
1. cAdvisor будет доступен по адресу `http://HOST_IP:8080`
1. Grafana будет доступна по адресу `http://HOST_IP:3000`

### Метрики

1. Сервис crawler
    - crawler_pages_parsed - количество обработанных ботом страниц
    - crawler_site_connection_time - время затраченное ботом на подключение к веб-сайту и загрузку веб-страницы
    - crawler_page_parse_time - время затраченное ботом на обработку содержимого веб-страницы
1. Сервис ui
    - web_pages_served - количество обработанных запросов
    - web_page_gen_time - время генерации веб-страниц, учитывая время обработки запроса
1. Метрики хоста из [node-exporter](https://hub.docker.com/r/prom/node-exporter)
1. Метрики докер контейнеров из [cAdvisor](https://hub.docker.com/r/google/cadvisor)

### Дашборды

В Grafana автоматически создаются дашборды с графиками по:
- метрикам хоста (на основе https://grafana.com/grafana/dashboards/6014)
- метрикам докер контейнеров (на основе https://grafana.com/grafana/dashboards/8321)
