provider "google" {
  version = "2.0.0"
  project = "${var.project}"
  region  = "${var.region}"
}

resource "google_compute_instance" "builder" {
  name         = "builder"
  machine_type = "n1-standard-1"
  zone         = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
    }
  }

  network_interface {
    network       = "default"
    access_config = {}
  }

  metadata {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }
}

resource "google_compute_instance" "dev" {
  name         = "dev"
  machine_type = "n1-standard-1"
  zone         = "${var.zone}"
  tags         = ["http", "prometheus", "cadvisor", "grafana"]

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
    }
  }

  network_interface {
    network       = "default"
    access_config = {}
  }

  metadata {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }
}

resource "google_compute_instance" "stage" {
  name         = "stage"
  machine_type = "n1-standard-1"
  zone         = "${var.zone}"
  tags         = ["http", "prometheus", "cadvisor", "grafana"]

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
    }
  }

  network_interface {
    network       = "default"
    access_config = {}
  }

  metadata {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }
}

resource "google_compute_instance" "prod" {
  name         = "prod"
  machine_type = "n1-standard-1"
  zone         = "${var.zone}"
  tags         = ["http", "prometheus", "cadvisor", "grafana"]

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
    }
  }

  network_interface {
    network       = "default"
    access_config = {}
  }

  metadata {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }
}

resource "google_compute_firewall" "firewall_http" {
  name = "http"
  network = "default"

  allow {
    protocol = "tcp"
    ports = ["80"]
  }

  target_tags = ["http"]
}

resource "google_compute_firewall" "firewall_prometheus" {
  name = "prometheus"
  network = "default"

  allow {
    protocol = "tcp"
    ports = ["9090"]
  }

  target_tags = ["prometheus"]
}

resource "google_compute_firewall" "firewall_cadvisor" {
  name = "cadvisor"
  network = "default"

  allow {
    protocol = "tcp"
    ports = ["8080"]
  }

  target_tags = ["cadvisor"]
}

resource "google_compute_firewall" "firewall_grafana" {
  name = "grafana"
  network = "default"

  allow {
    protocol = "tcp"
    ports = ["3000"]
  }

  target_tags = ["grafana"]
}
