output "builder_external_ip" {
  value = "${google_compute_instance.builder.network_interface.0.access_config.0.nat_ip}"
}

output "dev_external_ip" {
  value = "${google_compute_instance.dev.network_interface.0.access_config.0.nat_ip}"
}

output "stage_external_ip" {
  value = "${google_compute_instance.stage.network_interface.0.access_config.0.nat_ip}"
}

output "prod_external_ip" {
  value = "${google_compute_instance.prod.network_interface.0.access_config.0.nat_ip}"
}
