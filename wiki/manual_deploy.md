## Ручная сборка и деплой

В данном разделе описано, как всё собрать и задеплоить без GitLab CI.

### Подготовка докер образов

Переименовать `docker/.env.example` в `docker/.env`, прописав значение
`CI_REGISTRY_IMAGE` для своего хранилища докер образов.

Собрать и запушить все необходимые докер образы:
```shell script
source docker/.env && export CI_REGISTRY_IMAGE
make all
```

### Запуск сервисов приложения

Запустить сервисы приложения:
```shell script
cd docker
docker-compose up -d
```

Веб-интерфейс приложения будет доступен по адресу http://<HOST_IP>

### Запуск сервисов мониторинга

Запустить сервисы для мониторинга приложений:
```shell script
docker-compose -f docker-compose-monitoring.yml up -d
```
