## Ручная настройка ресурсов в GCP

Выбрать проект в GCP для создания ресурсов:
```shell script
export GOOGLE_PROJECT=<id проекта>
```

Добавить правила файервола:
```shell script
gcloud compute firewall-rules --project=$GOOGLE_PROJECT create http --allow tcp:80 --target-tags=http 
gcloud compute firewall-rules --project=$GOOGLE_PROJECT create prometheus --allow tcp:9090 --target-tags=prometheus
gcloud compute firewall-rules --project=$GOOGLE_PROJECT create cadvisor --allow tcp:8080 --target-tags=cadvisor
gcloud compute firewall-rules --project=$GOOGLE_PROJECT create grafana --allow tcp:3000 --target-tags=grafana
```

### Builder

Вспомогательная машинка для запуска тестов и сборки образов.

Создать хост:
```shell script
docker-machine create --driver google \
    --google-machine-image https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-1804-lts \
    --google-machine-type n1-standard-1 \
    --google-zone europe-west1-b \
    --google-tags http,prometheus,cadvisor,grafana \
    builder
```

Узнать адрес `HOST_IP` можно при помощи команды:
```shell script
docker-machine ip builder
```

Установить GitLab Runner:
```shell script
eval $(docker-machine env builder)

docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

### Тестирование

Зарегистрировать GitLab Runner для автоматического запуска тестов:
```shell script
export GITLAB_TOKEN=<токен для регистрации runner'ов>

docker exec -it gitlab-runner gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token $GITLAB_TOKEN \
  --executor docker \
  --description "test-python" \
  --docker-image "python:3.6.0-alpine" \
  --tag-list "test,python"
```

Unit-тесты для приложений crawler и ui запускаются в GitLab CI на каждый коммит.

### Сборка образов

Зарегистрировать GitLab Runner для сборки образов:
```shell script
export GITLAB_TOKEN=<токен для регистрации runner'ов>

docker exec -it gitlab-runner gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token $GITLAB_TOKEN \
  --executor docker \
  --description "build" \
  --docker-image "docker:19.03.1" \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --tag-list "build"
```

Docker образы собираются и пушатся в хранилище при каждом коммите в master.

### Dev окружение

Создать хост:
```shell script
docker-machine create --driver google \
    --google-machine-image https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-1804-lts \
    --google-machine-type n1-standard-1 \
    --google-zone europe-west1-b \
    --google-tags http,prometheus,cadvisor,grafana \
    dev
```

Установить GitLab Runner:
```shell script
eval $(docker-machine env dev)

docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

Зарегистрировать Runner для деплоя
```shell script
export GITLAB_TOKEN=<токен для регистрации runner'ов>

docker exec -it gitlab-runner gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token $GITLAB_TOKEN \
  --executor docker \
  --description "dev" \
  --docker-image "docker:19.03.1" \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --tag-list "dev"
```

Деплой на dev происходит при каждом коммите в master.

### Stage окружение

```shell script
docker-machine create --driver google \
    --google-machine-image https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-1804-lts \
    --google-machine-type n1-standard-1 \
    --google-zone europe-west1-b \
    --google-tags http,prometheus,cadvisor,grafana \
    stage
```

Установить GitLab Runner:
```shell script
eval $(docker-machine env stage)

docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

Зарегистрировать Runner для деплоя
```shell script
export GITLAB_TOKEN=<токен для регистрации runner'ов>

docker exec -it gitlab-runner gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token $GITLAB_TOKEN \
  --executor docker \
  --description "stage" \
  --docker-image "docker:19.03.1" \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --tag-list "stage"
```

Для деплоя на stage необходимо создать тег с версией (например, 1.0.0).
Деплой deploy-stage запускается вручную по кнопке в GitLab.

### Prod окружение

```shell script
docker-machine create --driver google \
    --google-machine-image https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-1804-lts \
    --google-machine-type n1-standard-1 \
    --google-zone europe-west1-b \
    --google-tags http,prometheus,cadvisor,grafana \
    prod
```

Установить GitLab Runner:
```shell script
eval $(docker-machine env prod)

docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

Зарегистрировать Runner для деплоя
```shell script
export GITLAB_TOKEN=<токен для регистрации runner'ов>

docker exec -it gitlab-runner gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token $GITLAB_TOKEN \
  --executor docker \
  --description "prod" \
  --docker-image "docker:19.03.1" \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --tag-list "prod"
```

Для деплоя на prod необходимо создать тег с версией (например, 1.0.0).
Деплой deploy-prod запускается вручную по кнопке в GitLab.
