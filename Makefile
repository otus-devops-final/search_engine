all: app monitoring

app: crawler ui

crawler: build-crawler push-crawler

build-crawler:
	docker build -t $(CI_REGISTRY_IMAGE)/crawler -f src/crawler/Dockerfile src/crawler/

push-crawler:
	docker push $(CI_REGISTRY_IMAGE)/crawler

ui: build-ui push-ui

build-ui:
	docker build -t $(CI_REGISTRY_IMAGE)/ui -f src/ui/Dockerfile src/ui/

push-ui:
	docker push $(CI_REGISTRY_IMAGE)/ui

monitoring: prometheus grafana

prometheus: build-prometheus push-prometheus

build-prometheus:
	docker build -t $(CI_REGISTRY_IMAGE)/prometheus -f monitoring/prometheus/Dockerfile monitoring/prometheus/

push-prometheus:
	docker push $(CI_REGISTRY_IMAGE)/prometheus

grafana: build-grafana push-grafana

build-grafana:
	docker build -t $(CI_REGISTRY_IMAGE)/grafana -f monitoring/grafana/Dockerfile monitoring/grafana/

push-grafana:
	docker push $(CI_REGISTRY_IMAGE)/grafana
